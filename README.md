# ** # NET.Core 3.1 / Angular9 / Fomantic UI / Covalent UI / ElectronJS ** #

1. NET.Core SDK
2. VSCODE


### Screenshots
![Dashboard](/images/2020-05-23_18_03_31-PostApp.png)

### Components

* https://material.angular.io/components
* https://teradata.github.io/covalent/
* https://material.angular.io/components/
* https://materialdesignicons.com/
* https://www.materialui.co/colors/
* https://fomantic-ui.com/
