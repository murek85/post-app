var electron = require('electron')
var app = electron.app;

if (handleSquirrelEvent(app)) {
    app.quit();
}

var path = require('path');
var url = require('url');
var BrowserWindow = electron.BrowserWindow;
var mainWindow;

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({ 
    width: 1280,
    height: 960,
    icon: url.format({
      pathname: path.join(__dirname, 'favicon.ico'),
      protocol: 'file:',
      slashes: true
    }),
    backgroundColor: '#B2DFDB'
  });
  mainWindow.setMenu(null);

  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }));

  // Event when the window is closed.
  mainWindow.on('closed', function () {
    mainWindow = null
  });
}

app.allowRendererProcessReuse = true;

// Create window on electron intialization
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {

  // On macOS specific close process
  if (process.platform !== 'darwin') {
    app.quit();
  }
})

app.on('activate', function () {
  // macOS specific close process
  if (mainWindow === null) {
    createWindow();
  }
})

function handleSquirrelEvent(app) {
  if (process.argv.length === 1) {
      return false;
  }
  var ChildProcess = require('child_process');
  var path = require('path');
  var appFolder = path.resolve(process.execPath, '..');
  var rootAtomFolder = path.resolve(appFolder, '..');
  var updateDotExe = path.resolve(path.join(rootAtomFolder, 'Update.exe'));
  var exeName = path.basename(process.execPath);
  var spawn = function (command, args) {
      var spawnedProcess, error;
      try {
          spawnedProcess = ChildProcess.spawn(command, args, {
              detached: true
          });
      }
      catch (error) { }
      return spawnedProcess;
  };
  var spawnUpdate = function (args) {
      return spawn(updateDotExe, args);
  };
  var squirrelEvent = process.argv[1];
  switch (squirrelEvent) {
      case '--squirrel-install':
      case '--squirrel-updated':
          // Optionally do things such as:
          // - Add your .exe to the PATH
          // - Write to the registry for things like file associations and
          //   explorer context menus
          // Install desktop and start menu shortcuts
          spawnUpdate(['--createShortcut', exeName]);
          setTimeout(app.quit, 1000);
          return true;
      case '--squirrel-uninstall':
          // Undo anything you did in the --squirrel-install and
          // --squirrel-updated handlers
          // Remove desktop and start menu shortcuts
          spawnUpdate(['--removeShortcut', exeName]);
          setTimeout(app.quit, 1000);
          return true;
      case '--squirrel-obsolete':
          // This is called on the outgoing version of your app before
          // we update to the new version - it's the opposite of
          // --squirrel-updated
          app.quit();
          return true;
  }
}