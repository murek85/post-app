import { Component, OnInit, AfterViewInit, AfterContentInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { TdLoadingService } from '@covalent/core/loading';
import { TranslateService } from '@ngx-translate/core';

import { AppConfig } from 'src/app/app.config';

import { AccountService } from 'src/app/core/services/account.service';

declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit, AfterContentInit {

  @ViewChild('header') headerRef: ElementRef;
  @ViewChild('menu') menuRef: ElementRef;

  system = { version: '1.0.0', date: '21-10-2019', type: 'dev' };
  languages: any[] = AppConfig.settings.i18n.availableLanguages;

  userName;
  password;
  email;

  error;

  constructor(
    private router: Router,
    private tdLoadingService: TdLoadingService,
    private translateService: TranslateService,
    public accountService: AccountService) { }

  @HostListener('window:scroll', ['$event'])
  scrollHandler(event): void {

    const headerHeight = this.headerRef.nativeElement.offsetHeight;
    const windowTop = event.target.scrollTop;

    if (headerHeight <= windowTop) {
      this.menuRef.nativeElement.classList.add('fade-in');
      this.menuRef.nativeElement.classList.remove('hidden');
    } else {
      this.menuRef.nativeElement.classList.add('hidden');
      this.menuRef.nativeElement.classList.remove('fade-in');
    }
  }

  private initSemanticConfig() {

    $('.ui.sidebar').sidebar();

    const language = JSON.parse(localStorage.getItem('language'));
    $('.languages').dropdown({
      on: 'hover',
      onChange: (value, text, $selectedItem) => {
        this.tdLoadingService.register();
        setTimeout(() => {
          localStorage.setItem('language', JSON.stringify(value));
          this.translateService.setDefaultLang(value);
          this.tdLoadingService.resolve();
        }, 500);
      }
    });

    $('.languages').dropdown('set selected',
      (language == null ? AppConfig.settings.i18n.defaultLanguage.code : language));
  }

  public ngAfterViewInit() {
    this.initSemanticConfig();
  }

  public ngAfterContentInit() {

  }

  public ngOnInit() {
    this.system = {
      version: AppConfig.settings.system.version,
      date: AppConfig.settings.system.date,
      type: AppConfig.settings.system.type
    };
  }

  public loginPasswordFlow() {}

  public reminderPassword() {}

  public loginSocialCode(providerKey) {}

  public send() {}

  public modalLogin() {
    $('.ui.modal.modal-login')
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false
      })
      .modal('show');
  }

  public modalPassword() {
    $('.ui.modal.modal-password')
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false
      })
      .modal('show');
  }

  public modalContact() {
    $('.ui.modal.modal-contact')
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false
      })
      .modal('show');
  }

  public modalRegulations() {
    $('.ui.modal.modal-regulations')
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false
      })
      .modal('show');
  }

  public modalPrivacy() {
    $('.ui.modal.modal-privacy')
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false
      })
      .modal('show');
  }

  public modalCookie() {
    $('.ui.modal.modal-cookie')
      .modal({
        context: 'td-layout',
        transition: 'fly left',
        closable: false
      })
      .modal('show');
  }
}
