import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, forkJoin } from 'rxjs';

import { UserModel } from 'src/app/shared/models/user.model';

import { NgxMAuthOidcService } from './oidc/oidc.service';

import { UsersHttpService } from './http/users.service';

import * as moment from 'moment';

@Injectable({ providedIn: 'root' })
export class AccountService {

  loginSocialProviders: any[] = [
    { name: 'Google', key: 'Google', icon: 'google', class: 'red' },
    { name: 'Facebook', key: 'Facebook', icon: 'facebook f', class: 'facebook' },
    { name: 'Twitter', key: 'Twitter', icon: 'twitter', class: 'twitter' }
  ];
  loginDomainProviders: any[] = [
    { name: 'Windows', key: 'Windows', icon: 'microsoft' }
  ];

  private userLoggedSource = new BehaviorSubject(null);

  userLogged = this.userLoggedSource.asObservable();

  changeUserLogged(userLogged: UserModel) {
    this.userLoggedSource.next(userLogged);
  }

  constructor(
    private router: Router,
    private usersHttpService: UsersHttpService,
    private oidcService: NgxMAuthOidcService) { }

  public getUserLogged(): Promise<object> {
    return new Promise((resolve, reject) => {
      forkJoin(
        this.oidcService.loadUserProfile(),
        this.loadUserLogged()
      )
      .subscribe(
        ([loadUserProfile, loadUserLogged]: [any, UserModel]) => {
          const user: UserModel = {
            id: loadUserProfile.sub,
            email: loadUserProfile.email,
            userName: loadUserProfile.email,
            firstName: loadUserLogged?.firstName,
            lastName: loadUserLogged?.lastName,
            fullName: loadUserLogged?.fullName,
            phoneNumber: loadUserLogged?.phoneNumber,
            lastValidLogin: loadUserLogged?.lastValidLogin,
            lastIncorrectLogin: loadUserLogged?.lastIncorrectLogin,
            logins: loadUserLogged.logins,
            roles: [
              { name: 'Administrator', value: 'CAL_ADMINISTRATOR' }
            ],
            permissions: [
              { name: 'Zarządzanie użytkownikami', value: 'CAL_ZARZADZANIE_UZYTKOWNICY' },
              { name: 'Zarządzanie rolami', value: 'CAL_ZARZADZANIE_ROLE' },
              { name: 'Zarządzanie uprawnieniami', value: 'CAL_ZARZADZANIE_UPRAWNIENIA' }
            ]
          };

          user.provider = this.loginSocialProviders.find(provider => provider.key === loadUserLogged.logins[0]);

          this.changeUserLogged(user);
          resolve(user);
        },
        err => reject(err)
      );
    });
  }

  public getLoggedIn(): boolean {
    return this.oidcService.hasValidAccessToken();
  }

  public loadUserLogged() {
    return this.usersHttpService.getUserLogged();
  }

  public loginPasswordFlow(
    email: string,
    password: string): Promise<object> {

    return new Promise((resolve, reject) => {
      this.oidcService.fetchTokenUsingPasswordFlow(email, password)
        .then(token => this.getUserLogged().then(_ => resolve(token)))
        .catch(err => reject(err.error));
    });
  }

  public loginDomainFlow(
    login: string,
    password: string): Promise<object> {

    return new Promise((resolve, reject) => {
      this.oidcService.fetchTokenUsingDomainFlow(login, password)
        .then(token => this.getUserLogged().then(_ => resolve(token)))
        .catch(err => reject(err.error));
    });
  }

  public loginDomainCode() {
    const params = { provider: 'Windows' };
    this.oidcService.initAuthorizationCode(params);
  }

  public loginSocialCode(providerKey) {
    const params = { provider: providerKey };
    this.oidcService.initAuthorizationCode(params);
  }

  public logout() {
    this.oidcService.logout(true);
    this.changeUserLogged(null);
    this.router.navigate(['/']);
  }
}
