import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

import { AppConfig } from 'src/app/app.config';

import { BaseHttpService } from './base.service';

import { PagingSettingsModel } from 'src/app/shared/models/paging.model';
import { FilterUserModel } from 'src/app/shared/models/filters/filter-user.model';

@Injectable()
export class UsersHttpService extends BaseHttpService {
  constructor(
    protected http: HttpClient) {

    super(http);
  }

  public getUsers(pagingSettings: PagingSettingsModel, filters: FilterUserModel) {

    const params = new HttpParams({
      fromObject: {
        'pageNumber': String(pagingSettings.pageNumber),
        'pageSize': String(pagingSettings.pageSize)
      }
    });

    return this.http.post(`${AppConfig.settings.system.apiUrl}/api/users/list`, { ...<any>filters }, { params });
  }

  public getUser(aspId) {
    return this.http.get(`${AppConfig.settings.system.apiUrl}/api/users/${aspId}`);
  }

  public createUser(user: any) {
    return this.http.post(`${AppConfig.settings.system.apiUrl}/api/users`, user);
  }

  public updateUser(user: any) {
    return this.http.put(`${AppConfig.settings.system.apiUrl}/api/users`, user);
  }

  public deleteUser(aspId) {
    return this.http.delete(`${AppConfig.settings.system.apiUrl}/api/users/${aspId}`);
  }

  public getUserLogged()  {
    return this.http.get(`${AppConfig.settings.system.apiUrl}/api/account/user`);
  }
}
